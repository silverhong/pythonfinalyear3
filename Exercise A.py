#A1
print("**********")
print("=========================")
#A2
for i in range(5):
    print("**********")
print("=========================")
i=0
#A3
for i in range(5):
    for y in range(i+1):
        print('*',end='')
    print('\n')
print("=========================")
i=0
y=1
z=0
#A4
for i in range(5):
    z=0
    while(z<y):
        print('*',end='')
        z+=1
    y *= 2
    print('\n')
print("=========================")
#A5
i=1
y=1
while(i<=5):
    y=1
    while(y<=5-i):
        print(' ',end='')
        y+=1
    z=5-i
    while(z<5):
        print('*',end='')
        z+=1
    print('\n')
    i+=1
print("=========================")
#A7
i=1
y=1
while(i<=9):
    y=1
    z = i
    while(y<=i):
        print(str(z),end='')
        z-=1
        y+=1
    print('\n')
    i+=1
print("=========================")
#A8
i=1
y=1
while(i<=9):
    y=1
    while(y<=i):
        print(str(y),end='')
        y+=1
    print('\n')
    i+=1
print("=========================")
#A9
i=1
y=1
while(i<=9):
    y=9
    z=y
    while(y>=i):
        print(str(z),end='')
        z-=1
        y-=1
    print('\n')
    i+=1
print("=========================")
#A10
i=1
y=1
while(i<=9):
    y=9
    z=1
    while(y>=i):
        print(str(z),end='')
        z+=1
        y-=1
    print('\n')
    i+=1
print("=========================")
#A11

