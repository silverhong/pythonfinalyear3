from datetime import datetime
from prettytable import PrettyTable
from dateutil.relativedelta import relativedelta
borrowMoney=int(input('Loan Money = $'))
rate=float(input('Rate = %'))/100
inputReceiveDate = input('Receive Date = ').split('-')
receiveDate = datetime(int(inputReceiveDate[2]),int(inputReceiveDate[1]),int(inputReceiveDate[0]))
inputPayDate=input('Pay Date = ').split('-')
payDate = datetime(int(inputPayDate[2]),int(inputPayDate[1]),int(inputPayDate[0]))
duration=int(input('Duration = '))
table =  PrettyTable(['Month','Pay Date','Loan Money','Rate Pay','Loan Pay','Pay Money'])
rateMoney=0.0
moneyAcumulate=borrowMoney/duration
payMoney=moneyAcumulate
for month in range(1,duration+1):
    rateMoney=borrowMoney*rate
    payMoney = rateMoney + moneyAcumulate
    table.add_row([str(month),payDate.strftime('%d-%m-%Y'),str(round(borrowMoney,2)),str(round(rateMoney,2)),str(round(moneyAcumulate,2)),str(round(payMoney,2))])
    payDate = payDate + relativedelta(months=1)
    borrowMoney-=moneyAcumulate


print(table)

# print(requestDate.strftime('%d-%m-%Y'))