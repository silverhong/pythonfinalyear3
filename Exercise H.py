n = input("Please input N=");

i = 1;
sum = 0;
print("sum = ",end='')
while (i<=int(n)):
    sum += i;
    print(str(i)+"+",end='')
    i += 1;
print("\b = "+str(sum))

i = 1;
sum = 0;
print("sum = ",end='')
while (i<=int(n)):
    sum += i;
    print(str(i)+"+",end='')
    i += 2;
print("\b = "+str(sum))

i = 1;
sum = 0;
print("sum = ",end='')
while (i<=int(n)):
    if(i%2==0 or i==1):
        sum += i;
        print(str(i)+"+",end='')
    i+=1
print("\b = "+str(sum))

i = 1;
sum = 1;
print("sum = ",end='')
while (i<=int(n)):
    sum *= i;
    print(str(i)+"*",end='')
    i += 1;
print("\b = "+str(sum))